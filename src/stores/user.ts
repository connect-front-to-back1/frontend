import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])
  
  async function getUser(id: number) {
    loadingStore.doload()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doload()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  
  async function saveUser(user: User) {
    loadingStore.doload()
    if (user.id < 0) {
      console.log('Post ' + JSON.stringify(user))
      const res = await userService.addUser(user)
    } else {
      console.log('Patch ' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }
  
  async function deleteUser(user: User) {
    loadingStore.doload()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }
  return { users, getUsers, saveUser, deleteUser, getUser}
})


