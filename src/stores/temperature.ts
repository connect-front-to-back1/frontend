import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperature from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()
 
  async function callConvert() {
    // result.value = convert(celsius.value)
    loadingStore.doload()
    try {
      
      result.value = await temperature.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
  }

  return { valid, result, celsius, callConvert }
})
